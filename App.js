
import './App.css';
import DemoHookUseCallback from './ReacHook/DemoHookUseCallback';
import DemoHookUseEffec from './ReacHook/DemoHookUseEffec';
import DemoHookUseMemo from './ReacHook/DemoHookUseMemo';
import DemoHookUseState from './ReacHook/DemoHookUseState';
import DemoUseRef from './ReacHook/DemoHookUseRef';
import DemoHookUseReducer from './ReacHook/DemoHookUseReducer';
import DemoHookUseContext from './ReacHook/DemoHookUseContext';
import ContextProvider from './ReacHook/acontext/ContextProvider';
import DemoReduxApp from './ReacHook/DemoReduxApp';
import DemoUseSpring from './ReacHook/reacSpring/DemoUseSpring';
function App() {
  return (
    // <ContextProvider >
    <div>
     {/* <DemoHookUseState /> */}
     {/* <DemoHookUseEffec /> */}
     {/* <DemoHookUseCallback /> */}
     {/* <DemoHookUseMemo /> */}
     {/* <DemoUseRef /> */}
     {/* <DemoHookUseReducer /> */}
     {/* <DemoHookUseContext /> */}
     {/* <DemoReduxApp /> */}
     <DemoUseSpring />
    {/* // </ContextProvider> */}
    </div>
  );
}

export default App;
