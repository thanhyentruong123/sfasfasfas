import React from 'react'


const context = React.createContext();

export default function ContextProvider(props) {
    return (
       <context.Provider>
           {props.children}
       </context.Provider>
    )
}
