import React, {memo} from 'react'

 function ChildUseMemo(props) {
    console.log('bi thay doi');
    return (
        
       <div className="container">
           <div className="table">
               <thead>
                   <tr>
                       <th>ID</th>
                       <th>Name</th>
                       <th>Price</th>
                   </tr>
                   
               </thead>
               <tbody>
                   {
                       props.card.map((item,index) => {
                           return (
                               <tr kay={index}>
                                   <th>{item.id}</th>
                                   <th>{item.name}</th>
                                   <th>{item.price}</th>
                               </tr>
                           )
                       })
                   }
               </tbody>
           </div>
       </div>
    )
}
export default memo(ChildUseMemo)
