import React,{useState, useMemo} from 'react'
import ChildUseMemo from './ChildUseMemo'

export default function DemoHookUseMemo(props) {
    let [like, setLike] = useState({like: 1})
    let card = [ 
        {id:1, name: 'oppo', price: 5000},
        {id:2, name: 'iphone', price: 2000},
        {id:3, name: 'samsung', price: 3000}
    ]
    const cardMemo = useMemo(()=> card, [])     //trả về 1 giá trị (card)
    return (
        <div classname="container">
            <div className="card text-left">
                <img style={{width:200, height:200}} className="card-img-top" src="https://m24htintuc.files.wordpress.com/2019/09/anh-gai-xinh-dep-hot-girl.jpg?w=644" alt='hello' />
                <div className="card-body">
                    <h4 className="card-title">Ha An</h4>
                    <p style={{color:'red'}}> {like.like} </p>
                    
                    <button className="btn btn-primary" onClick={()=> setLike({like: like.like+1})}>Like</button>
                </div>
               <ChildUseMemo card={cardMemo} />        
               {/* truyền vào card thì bên thằng con sẽ bị load lại, còn truyền vào cardMemo thì không */}
            </div>
        </div>
    )
}
