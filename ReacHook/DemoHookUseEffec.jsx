//là 1 hàm chạy sau khi render giao diện xong (luôn luôn chạy lần đầu)
import React, {useState, useEffect} from 'react'

export default function DemoHookUseEffec() {
    let [number, setNumber] = useState(1);
    let [like, setLike] = useState({like: 1})
    // const handerAdd = () => {
    //     return setNumber(number=number+1);
    // }
    //hàm useEffect là hàm chạy sau khi render và nó thay thế cho didupdate và didmount trong mọi TH khi không có []
    useEffect(() => {
    console.log('didmount');
    console.log('didupdate');
     return () => {                       //khi hàm này chạy thì sẽ xóa cái gì đó trong này (có thể bỏ qua) 
         console.log('willoutmout');   
     }
    },[like.like])
    // có [] nếu state nằm trong này thay đổi thì hàm effect sẽ chạy lại 
    //trong ví dụ này nếu state của like thay đổi mới chạy hàm useffect lại (nhấn like)
    //state của number thay đổi thì cũng k chạy hàm useffect 
    console.log('ahihi');



    return (
        <div className="container">
        <div className="card text-left col-3">
          <img style={{width:200, height:200}} className="card-img-top" src="https://genk.mediacdn.vn/thumb_w/690/2019/12/12/11-1575883882-width650height762-1576168363318789958220.jpg" alt='Hello' />
          <div className="card-body">
            <h4 className="card-title">Girl xinh</h4>
            <p className="card-text" style={{color:'red'}}>Tha tim {number}</p>
            <p className="card-text" style={{color:'blu'}}>Like {like.like}</p>
            <button className="btn btn-danger" onClick={() => setNumber(number=number+1)}>Tha Tim</button>
            <button className="btn btn-primary" onClick={()=> setLike({like: like.like+1})}>Like</button>
          </div>
        </div>
    </div>
    )
}
