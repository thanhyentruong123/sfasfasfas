import React,{useState, useCallback}  from 'react'
import ChildUseCallback from './ChildUseCallback';

export default function DemoHookUseCallback(props) {
    let [like, setLike] = useState({like:0})
    const renderNotify = ()=> {
        return `ban da tha ${like.like}`
    }
    // const callbackRenderNotify = useCallback(
    //     () => {
    //         callback                  nhận vào 2 tham số. tham số 1 là 1 hàm
     //                                   tham số 2 là đầu bị thay đổi
    //     [input],                       nếu bị bỏ trống thì sẽ k bao giờ render lại
    // )
    const callbackRenderNotify = useCallback(renderNotify,[like.like])
    return (
        <div classname="container">
            <div className="card text-left">
                <img style={{width:200, height:200}} className="card-img-top" src="https://m24htintuc.files.wordpress.com/2019/09/anh-gai-xinh-dep-hot-girl.jpg?w=644" alt='hello' />
                <div className="card-body">
                    <h4 className="card-title">Ha An</h4>
                    <p style={{color:'red'}}> {like.like} </p>
                    
                    <button className="btn btn-primary" onClick={()=> setLike({like: like.like+1})}>Like</button>
                </div>
                <ChildUseCallback renderNotify={callbackRenderNotify}/>
            </div>
        </div>
    );
}
