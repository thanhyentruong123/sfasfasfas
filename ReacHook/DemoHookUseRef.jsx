import React, {useRef, useState} from 'react'

export default function DemoHookUseRef() {
    let [login, setLogin] = useState({useName:''});
    // cachs1 : dung useRef để Dom tới thẻ nào đó. lấy giá trị,....
    let inputUseName = useRef (null);
    let inputPassWord = useRef (null);
    //cách 2. dùng useRef để lưu giá trị nào đó.
    let useName =useRef('');
    const handerLogin = () => {
        // console.log(inputUseName.current.value);
        // let {name} = inputPassWord.current ;
        // console.log(name) ;
        console.log('useName.current', useName.current);
        console.log('login.useName', login.useName);
        useName.current = 'abc';
        setLogin({
           useName: useName.current
        })
    }
    return (
       <div className="container text-left">
           <h3>Logins</h3>
           <div className="form-group ">
               <h3>UseName</h3>
               <input  ref={inputUseName} style={{width: 200}} name="useName" type="text" className="form-control" />
           </div>
           <div className="form-group">
               <h3>PassWord</h3>
               <input ref={inputPassWord} style={{width: 200}} name="PassWord" type="text" className="form-control" />
           </div>
           <div className="form-group">
               <button className="btn btn-success" onClick={() => {handerLogin()}}>Login</button>
           </div>
           
       </div>
    )
}
