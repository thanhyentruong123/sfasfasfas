import React, {useState,useRef} from 'react';
import {connect, useDispatch,useSelector} from 'react-redux';
import { addNewHobby } from '../reducer/actionFb';
export default function DemoReduxApp(props) {
  // dùng useSelector để lấy thông tin từ store về
    let comments = useSelector (state => state.faceBookReducer.comments);
    // console.log(comments);
    //dung useDispatch de gui gia tri len FacebookReducer
    let dispatch = useDispatch ();
    //lấy thông tin từ người dùng nhập vào
    let [useContent, setUseContent] = useState ({
      name: '',
      content: '', 
      avatar: ''
    });
    let inputUseName = useRef (null);
    let inputPassWord = useRef (null);
    console.log('a', inputUseName.current);
    const handleComments = () => {
       console.log('ss', inputUseName.current)
      const action = addNewHobby();
      dispatch(action);
    }
    
    // lấy thông tin từ người dùng nhập vào
    // let [useContent, setUseContent] = useState ({
    //   name: '',
    //   content: '', 
    //   avatar: ''
    // })
    // const handleChange = (e) => {
    //   let {value, name} = e.target;
    //   setUseContent({
    //     ...useContent,
    //     [name]: value
    //   })
    // }
    // //submit thong tin nguoi dung len faceBookReducer
    // const handleComments = (e) => {
    //   e.preventDefault(); //chan browser reloat
    //  let usContent = {...useContent,avatar: `https://photo-cms-kienthuc.zadn.vn/zoom/800/uploaded/nguyenanhson/2020_05_21/2/tha-thinh-sieu-ngot-hot-girl-2k3-so-huu-trang-ca-nhan-trieu-follow-hinh-7.jpg`};
    //   let action = {
    //     type: 'add_content', 
    //     useContent: usContent,
    //   }
    //   dispatch(action)
    // }
    return (
      <div className="container">
        <h2>FaceBook App</h2>
        <div className="card text-left">
           <div className="card-header">
           
               <div className="row mb-5">
                   <div className="col-2">
                     <img src="https://m24htintuc.files.wordpress.com/2019/09/anh-gai-xinh-dep-hot-girl.jpg?w=644" 
                     alt="a" height= {100} />
                   </div>
                   <div className="col-8 mt-3"  >
                       <h5 className="text-danger"> Yasuo </h5>
                       <p> Hi. tôi là Yasuo nè </p>
                   </div>
               </div>
               <div className="row mb-5">
                   <div className="col-2">
                     <img src="https://m24htintuc.files.wordpress.com/2019/09/anh-gai-xinh-dep-hot-girl.jpg?w=644" 
                     alt="a" height= {100} />
                   </div>
                   <div className="col-8 mt-3"  >
                       <h5 className="text-danger"> Yasuo </h5>
                       <p> Hi. tôi là Yasuo nè </p>
                   </div>
               </div>
               <div className="row mb-5">
                   <div className="col-2">
                     <img src="https://m24htintuc.files.wordpress.com/2019/09/anh-gai-xinh-dep-hot-girl.jpg?w=644" 
                     alt="a" height= {100} />
                   </div>
                   <div className="col-8 mt-3"  >
                       <h5 className="text-danger"> Yasuo </h5>
                       <p> Hi. tôi là Yasuo nè </p>
                   </div>
               </div>
               {comments.map((comments, index) => {
            return <div className="row mb-5" kay={index}>
                      <div className="col-2">
                     <img src={comments.avatar} 
                     alt="a" height= {100} style={{width: "75%"}} />
                   </div>
                   <div className="col-8 mt-3"  >
                       <h5 className="text-danger">{comments.name}</h5>
                       <p> {comments.content} </p>
                   </div>
            </div>
          })}
           </div>
           <div >
               <form action="#" className="card-body" onSubmit={handleComments} >
                    <div className="form-group">
                        <h4>Name</h4>
                        <input ref={inputUseName} type="text" className='form-control' name='name'  />
                    </div>
                    <div className="form-group">
                        <h4>Content</h4>
                        <input ref={inputPassWord} type="text" className='form-control' name='content' />
                    </div>
                    <div className="form-group">
                      <button className="btn btn-success" >Gửi</button>
                    </div>

               </form>
             
           </div>
        </div>
      </div>
    );
}
// const mapStateToProps = (state) => {
//     return {
//         comments: state.faceBookReducer.comments
//     }
// }
// export default connect (mapStateToProps, null)(DemoReduxApp);