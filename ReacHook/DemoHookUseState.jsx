import React, {useState} from 'react'

export default function DemoHookUseState() {
    // 1: this.state
    //2: this.setState
    let [state, setState] = useState({like: 0 });
    const handerLike = () => {
        return  setState({like: state.like + 1})
    }
    return (
      <div className="container">
        <div className="card text-left">
          <img style={{width:200, height:200}} className="card-img-top" src="https://genk.mediacdn.vn/thumb_w/690/2019/12/12/11-1575883882-width650height762-1576168363318789958220.jpg" alt='Hello' />
          <div className="card-body">
            <h4 className="card-title">Girl xinh</h4>
            <p className="card-text" style={{color:'red'}}>Like {state.like}</p>
            <button className="btn btn-danger" onClick={handerLike}>Tha Tim</button>
          </div>
        </div>
    </div>
    );
}
