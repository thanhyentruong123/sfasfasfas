import React, {useReducer} from 'react'

const initialCart =[ 
    {id: 1, name: 'Hello', price: 1000, soLuong: 1},
    
]
let arrProduct = [ 
    {id: 1, name: 'Hello', price: 1000},
    {id: 2, name: 'vivo', price: 2000},
    {id: 3, name: 'samsung', price: 5000},
]
const cartReducer = (state, action) => {
    //  // eslint-disable-next-line default-case
     // eslint-disable-next-line default-case
     switch (action.type) {
         case 'themGioHang': {
             let cartUpdate = [...state];
             let index = cartUpdate.findIndex(itemCart => itemCart.id === action.item.id);
             if (index !== -1) {
                 cartUpdate[index].soLuong+=1;
             } else {
                 const itemCart = {...action.item, soLuong: 1};
                 cartUpdate.push(itemCart)
             }
             
             return cartUpdate;
         }
     }



      return [...state]
}

export default function DemoHookUseReducer(props ) {
    let  [cart, dispatch] = useReducer(cartReducer, initialCart);  //let [state,setState] = 
    const themGioHang = (itemClick) => {
        console.log(itemClick,'fafa');
        const action = {
            type: 'themGioHang', 
            item: itemClick
        };
        dispatch(action)
    }


    return (
        <div className="container"> 
           <div className="row mb-5">
               {arrProduct.map((sp, index) => {
                   return (
                     <div className="col-4" key={index}>
                       <div className="card text-left">
                         <img
                           className="card-img-top"
                           src="https://m24htintuc.files.wordpress.com/2019/09/anh-gai-xinh-dep-hot-girl.jpg?w=644"
                           alt='aaa'
                         />
                         <div className="card-body">
                           <h4 className="card-title">{sp.name}</h4>
                           <p className="card-text">{sp.price}</p>
                           <button className="btn btn-primary"  onClick={() =>{themGioHang(sp)} }>Them Gio Hang</button>
                         </div>
                       </div>
                     </div>
                   );
               })}
           </div>
           <h2 style={{color: 'red'}} >Giỏ hàng</h2>
           <div className="table">
               <thead>
                   <tr>
                       <th>ID</th>
                       <th>Name</th>
                       <th>Price</th>
                       <th>SoLuong</th>
                   </tr>
               </thead>
               <tbody>
                  
                      {cart.map((item, index) => {
                          return <tr kay={index}>
                                <th>{item.id}</th>
                                <th>{item.name}</th>
                                <th>{item.price}</th>
                                <th>{item.soLuong}</th>
                                <th><button className="btn btn-danger">Xoa</button></th>
                          </tr>
                      })}
                   
               </tbody>
           </div>
        </div>
    )
}
